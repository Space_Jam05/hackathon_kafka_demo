# How to start up the docker spotify container.
Useful link: https://hub.docker.com/r/spotify/kafka/

### Install docker
https://docs.docker.com/install/
-- Note: I had run through https://docs.docker.com/get-started/ before I started this project.

### Add following line to /etc/hosts (as admin)
Windows: `C:\Windows\System32\drivers\etc\hosts`
Linux: `\etc\hosts`
Add `127.0.0.1       kafka`

### Run the K afka Server in Docker container
In /kafka_server run `docker-compose up -d`
-d runs it detached
 
### Create topic (prefix with winpty if it git bash)
`docker exec -it kafkaserver_kafka_1 ./opt/kafka_2.11-0.10.1.0/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic testTopic`

### List Topics to verify
`docker exec -it kafkaserver_kafka_1 ./opt/kafka_2.11-0.10.1.0/bin/kafka-topics.sh --list --zookeeper localhost:2181`

### Start Kafka Producer
Run KafkaHellowWorld in the /kafka_producer project 
Or from /kafka_producer:
`./gradlew run`

### Start Kafka Consumer
Run KafkaHellowWorld in the /kafka_consumer project 
Or from /kafka_consumer:
`./gradlew run`



### Other Useful Commands while I wrote this
#### See Running Docker Containers
docker ps -a
#### Stop all containers
docker kill $(docker ps -q)
#### Remove all containers
docker rm $(docker ps -aq)
#### View container logs
docker-compose logs -f kafka
### Get shell access to running container
winpty docker exec -it kafka_server_kafka_1 bash
### Create topic 
./kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic <TOPIC>
### List Topics
./kafka-topics.sh --list --zookeeper localhost:2181
### See Topics Remotely
winpty docker exec -it kafkaserver_kafka_1 /opt/kafka_2.11-0.10.1.0/bin/kafka-topics.sh --list --zookeeper localhost:2181

### Get ip of last docker container run
docker inspect  $(docker ps -q) | grep IPAddress



### Some useful links
https://data-flair.training/blogs/kafka-topic-architecture/
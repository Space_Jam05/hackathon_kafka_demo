import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Collections;
import java.util.Properties;

//http://cloudurable.com/blog/kafka-tutorial-kafka-consumer/index.html

public class KafkaHelloWorld {
    private final static String TOPIC = "testTopic";
    private final static String BOOTSTRAP_SERVERS =
//            "localhost:9092,localhost:9093,localhost:9094";
            "kafka:9092";

    private static Consumer<Long, String> createConsumer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                BOOTSTRAP_SERVERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "KafkaExampleConsumer");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                LongDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class.getName());

        Consumer<Long, String> consumer = new KafkaConsumer<Long, String>(props);
        consumer.subscribe(Collections.singleton(TOPIC));
        return consumer;
    }

    public static void runConsumer() {
        final Consumer<Long, String> consumer = createConsumer();
        int giveUp = 10;
        int noRecordsCount = 0;

        while(noRecordsCount < giveUp) {
            System.err.println("Attempting to get records!");
            final ConsumerRecords<Long,String> consumerRecords = consumer.poll(100);

            if(consumerRecords.count()==0) {
                noRecordsCount++;
            } else {
                consumerRecords.forEach( r -> {
                    System.out.printf("Consumer record: (%d, %s, %d, %d)\n",
                            r.key(), r.value(), r.partition(), r.offset());
                });
            }
            consumer.commitAsync();;
        }

        consumer.close();
        System.out.println("Done Consuming!!!");
    }

    public static void main(String ... arg) {
        try {
            runConsumer();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }
}